<?php
    $config_path = get_stylesheet_directory() . '/config/';

    $config = new CWP_Config( $config_path );

    class Book_Info extends CWP_Metabox {

        public function __construct( $book, $id, $title ) {
            parent::__construct( $book, $id, $title );
        }

        public function fields() {
            $book = $this->getPostType();

            $title = $book->get('title');
            $author = $book->get('author');
            $pages = $book->get('pages');

            echo "
                <input type='text' name='title' placeholder='Book title' value='".$title."' />
                <input type='text' name='author' placeholder='Book author' value='".$author."' />
                <input type='number' min='1' name='pages' placeholder='Book pages' value='".$pages."' />
            ";
        }
    }

    $book = new CWP_Post_Type( 'book', $config );

    $book_info_metabox = new Book_Info( $book, 'book-info', 'Book Info' );
