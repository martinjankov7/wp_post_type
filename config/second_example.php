<?php
/**
 * Config File name represents the post type.
 * Each array value represents custom meta key
 * Each array key is form fields name attribute and used for getting the value
 */

// Add labels for fields and metaboxes
return  array(
	// optional
	'name' => 'Post Type Name',

	'menu_position' => 5,

	'public' => true,

	'supports' => array(
					'title',
					'editor',
					'thumbnail'
				),

	'with_capabilities' => false,

	'metaboxes' => array(
		'second-example-info' => array(
			'title' => 'Second Example Info',

			'position' => 'normal', // if not set default is normal

			'priority' => 'high', // if not set default is high

			'fields' => array(

				'second_last_name' => array(
					'type' => 'text',
					'label' => 'Last Name',
					// 'meta_key' => '_name', // set meta key to be _{field_name}
					'class' => 'last-night' // optional (by default wpt-fieldname) doesn't overwrite default
				),

				'second_first_name' => array(
					'type' => 'text',
					'label' => 'First Name',
					// 'meta_key' => '_name', // set meta key to be _{field_name}
					'class' => 'first-name' // optional (by default posttype-fieldname) doesn't overwrite default
				),

				'second_age' => array(
					'type' => 'number',
					'label' => 'Age',
					'min' => 2,
					// 'meta_key' => '_name', // set meta key to be _{field_name}
					'class' => 'age' // optional (by default posttype-fieldname) doesn't overwrite default
				),

				'second_list' => array(
					'type' => 'select', // make this dynamic to to able to add taxonomies from another post type
					// 'meta_key' => '_list', // or maybe add function to add later. Make all custom post type available for outside
					// 'source' => 'some_taxonomy_or_whatever',
					// 'source_type' => 'taxonomies|terms whatever', // if source set options is not needed
					'options' => array( // use
						'value1' => 'title1',
						'value2' => 'title2',
					)
				)
			)
		),
	)
);
