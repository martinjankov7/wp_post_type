<?php
/**
 * Config File name represents the post type.
 * Each array value represents custom meta key
 * Each array key is form fields name attribute and used for getting the value
 */

// Add labels for fields and metaboxes
return  array(
	// optional
	'name' => 'Post Type Name',

	'menu_position' => 5,

	'public' => true,

	'supports' => array(
					'title',
					'editor',
					'thumbnail'
				),

	'with_capabilities' => false,

	'metaboxes' => array(
		'vouchers-example-info' => array(
			'title' => 'Second Example Info',

			'position' => 'normal', // if not set default is normal

			'priority' => 'high', // if not set default is high

			'fields' => array(
				'vouchers_first_name' => array(
					'type' => 'text',
					'label' => 'First Name',
					// 'meta_key' => '_name', // set meta key to be _{field_name}
					'class' => 'first-name' // optional (by default posttype-fieldname) doesn't overwrite default
				),

				'vouchers_age' => array(
					'type' => 'number',
					'label' => 'Age',
					'min' => 2,
					// 'meta_key' => '_name', // set meta key to be _{field_name}
					'class' => 'age' // optional (by default posttype-fieldname) doesn't overwrite default
				)
			)
		),
	),

	'taxonomies' => array(
		'voucher_banana' => array(
			'query_var'         => true,

			'publicly_queryable' => true,

			'rewrite'           => array( 'slug' => 'voucher_banana' ),
		),
		'voucher_banana2' => array(
			'query_var'         => true,

			'publicly_queryable' => true,

			'rewrite'           => array( 'slug' => 'voucher_banana2' ),
		),
	)
);
