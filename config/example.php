<?php
/**
 * Config File name represents the post type.
 * Each array value represents custom meta key
 * Each array key is form fields name attribute and used for getting the value
 */

// Add labels for fields and metaboxes
return  array(
	// optional
	'name' => 'Post Type Name',

	'menu_position' => 5,

	'public' => true,

	'supports' => array(
					'title',
					'editor',
					'thumbnail'
				),

	'with_capabilities' => false,

	'metaboxes' => array(
		'example-info' => array(
			'title' => 'Example Info',

			'position' => 'normal', // if not set default is normal

			'priority' => 'high', // if not set default is high

			'fields' => array(

				'last_name' => array(
					'type' => 'text',
					'label' => 'Last Name',
					// 'meta_key' => '_name', // set meta key to be _{field_name}
					'class' => 'last-night' // optional (by default wpt-fieldname) doesn't overwrite default
				),

				'first_name' => array(
					'type' => 'text',
					'label' => 'First Name',
					// 'meta_key' => '_name', // set meta key to be _{field_name}
					'class' => 'first-name' // optional (by default posttype-fieldname) doesn't overwrite default
				),

				'list' => array(
					'type' => 'select', // make this dynamic to to able to add taxonomies from another post type
					// 'meta_key' => '_list', // or maybe add function to add later. Make all custom post type available for outside
					'source_type' => 'taxonomy', // if source set options is not needed
					'source' => 'voucher_banana',
					// 'options' => array( // use
					// 	'value1' => 'title1',
					// 	'value2' => 'title2',
					// )
				),

				'vouchers' => array(
					'type' => 'select', // make this dynamic to to able to add taxonomies from another post type
					'source_type' => 'post_type', // if source set options is not needed
					'source' => 'vouchers',
					'none_option' => true
				)
			)
		),

		'example-sidebar' => array(
			'title' => 'Example Sidebar',

			'position' => 'side', // if not set default is normal

			'priority' => 'high', // if not set default is high

			'fields' => array(

				'max_value' => array(
					'type' => 'number',
					'label' => 'Max Value',
					'min' => 10,
					// 'meta_key' => '_name', // set meta key to be _{field_name}
					'class' => '' // optional (by default wpt-fieldname) doesn't overwrite default
				),

				'min_value' => array(
					'type' => 'number',
					'label' => 'Min Value',
					'min' => 1,
					// 'meta_key' => '_name', // set meta key to be _{field_name}
					'class' => 'first-name' // optional (by default posttype-fieldname) doesn't overwrite default
				),

				'color' => array(
					'type' => 'color',
					'label' => 'Color',
					// 'meta_key' => '_name', // set meta key to be _{field_name}
					'class' => 'first-name' // optional (by default posttype-fieldname) doesn't overwrite default
				),
			)
		),
	),

	'taxonomies' => array(
		'example_categorie' => array(
			'hierarchical'      => true,

			'show_ui'           => true,

			'show_admin_column' => true,

			'query_var'         => true,

			'publicly_queryable' => true,

			'rewrite'           => array( 'slug' => 'example_categories' ),
		)
	)
);
