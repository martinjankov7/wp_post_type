<div class="wrap">
	<h2>Settings</h2>
	<form method="post" action="options.php">
	    <?php settings_fields( 'wpt-settings-group' ); ?>
	    <?php do_settings_sections( 'wpt-settings-group' ); ?>
	    <h3>Configuration caching</h3>
	    <table class="form-table">
        <tr valign="top">
	        <td>
	        	<label for="wpt_transient_cache">
		        	<input type="radio" id="wpt_transient_cache" name="wpt_config_cache" value="transient" <?php checked( get_option('wpt_config_cache'), 'transient' ); ?>/>
	        		Use transient for caching configuration files
	        	</label>
	        </td>
	        <td>
	        	<label for="wpt_memory_cache">
		        	<input type="radio" id="wpt_memory_cache" name="wpt_config_cache" value="memcache" <?php checked( get_option('wpt_config_cache'), 'memcache' ); ?>/>
		        	Use memcache for caching configuration files
	        	</label>
	        </td>
        </tr>
				<tr valign="top">
					<th scope="row">Memcache server IP/Host</th>
	        <td>
	        	<input type="text" name="wpt_memcache_host" value="<?php echo get_option('wpt_memcache_host')?>" />
	        </td>
	      </tr>
	      <tr>
					<th scope="row">Memcache server port</th>
	        <td>
	        	<input type="text" name="wpt_memcache_port" value="<?php echo get_option('wpt_memcache_port')?>" />
	        </td>
				</tr>
    	</table>
    <?php submit_button(); ?>
	</form>
</div>
