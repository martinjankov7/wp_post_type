<?php
/**
 * CWP_Options deals with reading static options this plugin may or may not have
 *
 * @package  WPPostType
 */

class CWP_Options {
	public function getConfigrationCaching() {
		return get_option('wpt_config_cache');
	}

	public function isMemcacheCaching(){
		return $this->getConfigrationCaching() === 'memcache';
	}

	public function isTransientCaching(){
		return $this->getConfigrationCaching() === 'transient';
	}

	public function getMemcacheHost(){
		return get_option('wpt_memcache_host');
	}

	public function getMemcachePort(){
		return get_option('wpt_memcache_port');
	}
}
