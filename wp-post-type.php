<?php
/**
 * Plugin Name: WP Post Type
 * Description: Create custom post types.
 * Author:      Martin Jankov
 * Author URI:  https://mk.linkedin.com/in/martinjankov
 * Version:     1.0.0
 * Text Domain: wpt
 *
 * WP Post Type is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * WP Post Type is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WP Post Type. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    WPPostType
 * @author     Martin Jankov
 * @since      1.0.0
 * @license    GPL-3.0+
 * @copyright  Copyright (c) 2018, Martin Jankov
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

final class WPPostType {

	private static $_instance;

	private $_version = '1.0.0';

	public $config;

	public $options;

	public static function instance() {
		if ( ! isset( self::$_instance ) && ! ( self::$_instance instanceof WPPostType ) ) {
			self::$_instance = new WPPostType;
			self::$_instance->constants();
			self::$_instance->includes();

			add_action( 'plugins_loaded', array( self::$_instance, 'objects' ), 10 );
			add_action( 'admin_enqueue_scripts', array( self::$_instance, 'wpt_register_admin_scripts' ) );
		}

		return self::$_instance;
	}

	private function includes() {
		// Fields
		require_once WPT_PLUGIN_DIR . 'core/fields/CWP_Fields.php';


		// Interfaces.
		require_once WPT_PLUGIN_DIR . 'core/interfaces/CWP_Post_Type_Interface.php';

		// Config.
		require_once WPT_PLUGIN_DIR . 'core/CWP_Config.php';

		// Metabox.
		require_once WPT_PLUGIN_DIR . 'core/CWP_Metabox.php';

		// Taxonomie.
		require_once WPT_PLUGIN_DIR . 'core/CWP_Taxonomy.php';

		// Post Type.
		require_once WPT_PLUGIN_DIR . 'core/post_type/CWP_Post_Type.php';
		require_once WPT_PLUGIN_DIR . 'core/post_type/CWP_Post_Type_Register.php';
		require_once WPT_PLUGIN_DIR . 'core/post_type/CWP_Post_Type_Register_Taxonomies.php';

		require_once WPT_PLUGIN_DIR . 'helpers/CWP_Options.php';

		if ( is_admin() ) {
			require_once WPT_PLUGIN_DIR . 'admin/CWP_Admin_Dashboard.php';
		}
	}

	private function constants() {
		// Plugin version.
		if ( ! defined( 'WPT_VERSION' ) ) {
			define( 'WPT_VERSION', $this->_version );
		}

		// Plugin Folder Path.
		if ( ! defined( 'WPT_PLUGIN_DIR' ) ) {
			define( 'WPT_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
		}

		// Plugin Folder URL.
		if ( ! defined( 'WPT_PLUGIN_URL' ) ) {
			define( 'WPT_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
		}

		// Config Folder Path.
		if ( ! defined( 'WPT_CONFIG_DIR' ) ) {
			define( 'WPT_CONFIG_DIR', plugin_dir_path( __FILE__ ) . '/config' );
		}
	}

	public function objects() {

		if ( is_admin() ) {
			new CWP_Admin_Dashboard();
			$this->fields = new CWP_Fields();
		}

		$this->options = new CWP_Options();
		$this->config = new CWP_Config( WPT_CONFIG_DIR );
	}

	public function wpt_register_admin_scripts() {
		wp_register_style( 'wpt-metabox-style', WPT_PLUGIN_URL . '/assets/admin/css/metabox.css', null, WPT_VERSION );
		wp_enqueue_style( 'wpt-metabox-style' );
	}
}

function wpt() {
	return WPPostType::instance();
}
wpt();
