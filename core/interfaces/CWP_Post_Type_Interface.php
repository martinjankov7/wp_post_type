<?php
/**
 * Interace for CWP_Post_Type
 *
 * @package WPPostType
 */
interface CWP_Post_Type_Interface {
	/**
	 * Add metabox in metabox array
	 * @param CWP_Metabox $metabox
	 */
	public function addMetabox( CWP_Metabox $metabox );

	/**
	 * Return the metaboxes array
	 * @return array
	 */
	public function getMetaboxes();

	/**
	 * Triggers on save post
	 * @param  int     $post_id
	 * @param  WP_Post $post
	 * @param  bool    $update
	 * @return void
	 */
	public function onSave( int $post_id, WP_Post $post, bool $update );

	/**
	 * Get post type custom field value
	 * @param  string $field
	 * @param  int $ID
	 * @return mixed
	 */
	public static function get( string $field, int $ID = null );
}
