<?php
/**
 * CWP_Metabox class allows creating metabox
 *
 * @package  WPPostType
 */

class CWP_Metabox {
	/**
	 * Post type for metabox
	 * @var CWP_Post_Type
	 */
	private $_post_type;

	/**
	 * Metabox wrapper id
	 * @var string
	 */
	private $_id;

	/**
	 * Title of the metabox
	 * @var string
	 */
	private $_title;

	/**
	 * Where to show the metabox (normal,side, advanced)
	 * @var string
	 */
	private $_position;

	/**
	 * Priority where the boxes should show (high, low, default)
	 * @var string
	 */
	private $_priority;

	/**
	 * Data to be set as the args property of the metabox
	 * @var array
	 */
	private $_callback_args;

	/**
	 * Metabox fields
	 * @var array
	 */
	private $_fields;

	/**
	 * Initialize the metabox
	 * @param CWP_Post_Type $post
	 * @param array $fields
	 * @param string $id
	 * @param string $title
	 * @param string $position
	 * @param string $priority
	 * @param mixed $callback_args
	 */
	public function __construct( CWP_Post_Type $post, array $fields = array(), $id, $title, $position = 'normal', $priority = 'high', $callback_args = null ) {
		$this->setPostType( $post );
		$this->setId( $id );
		$this->setTitle( $title );
		$this->setPosition( $position );
		$this->setPriority( $priority );
		$this->setCallbackArgs( $callback_args );
		$this->setFields( $fields );
	}

	/**
	 * Set the post type of metabox
	 * @param CWP_Post_Type $post
	 */
	public function setPostType( CWP_Post_Type $post ) {
		$this->_post_type = $post;
		$this->_post_type->addMetabox( $this );
	}

	/**
	 * Set the id of metabox
	 * @param string $id
	 */
	public function setId( $id ) {
		$this->_id = $id;
	}

	/**
	 * Set the title of metabox
	 * @param string $title
	 */
	public function setTitle( $title ) {
		$this->_title = $title;
	}

	/**
	 * Set the position of metabox
	 * @param string $position
	 */
	public function setPosition( $position ) {
		$this->_position = $position;
	}

	/**
	 * Set the priority of metabox
	 * @param string $priority
	 */
	public function setPriority( $priority ) {
		$this->_priority = $priority;
	}

	/**
	 * Set the callback arguments of metabox
	 * @param string $callback_args
	 */
	public function setCallbackArgs( $callback_args ) {
		$this->_callback_args = $callback_args;
	}

	/**
	 * Set fields
	 * @param string $callback_args
	 */
	public function setFields( $fields ) {
		$this->_fields = $fields;
	}

	/**
	 * Get the post type of metabox
	 * @return string
	 */
	public function getPostType() {
		return $this->_post_type;
	}

	/**
	 * Get the id of metabox
	 * @return string
	 */
	public function getId() {
		return $this->_id;
	}

	/**
	 * Get the title of metabox
	 * @return string
	 */
	public function getTitle() {
		return $this->_title;
	}

	/**
	 * Get the position of metabox
	 * @return string
	 */
	public function getPosition() {
		return $this->_position;
	}

	/**
	 * Get the priority of metabox
	 * @return string
	 */
	public function getPriority() {
		return $this->_priority;
	}

	/**
	 * Get the callback arguments of metabox
	 * @return array
	 */
	public function getCallbackArgs() {
		return $this->_callback_args !== null ? $this->_callback_args : array();
	}

	/**
	 * Get the fields for the metabox
	 * @return array
	 */
	public function getFields() {
		return $this->_fields;
	}

		/**
	 * Which field you need to show the metabox, template or view of metabox section
	 * This function needs to be filled in child classes
	 *
	 * @return mixed
	 */
	public function fields() {
		$html = wpt()->fields->create( $this->getFields(), $this->_post_type->getPostTypeName() )->getHtml();

		echo $html;
	}
}
