<?php
/**
 * CWP_Config load config files and lets you work with them
 * You can add additional config files in different directorium and load it using this class
 * You can create child class and do other things there
 *
 * @package  WPPostType
 */

class CWP_Config {
	/**
	 * Config files path
	 * @var string
	 */
	private $_config_path;

	/**
	 * Array keeping config attributes
	 * @var array
	 */
	private $_config;

	/**
	 * Custom post type object
	 * @var array
	 */
	private $_post_type_obj;

	/**
	 * Keeps all post type fields
	 * @var array
	 */
	private $_post_type_fields;

	/**
	 * Initialize the class
	 * @param string $path
	 * @return  void
	 */
	public function __construct( string $path ) {
		$this->_config = array();
		$this->_post_type_fields = array();
		$this->setPath( $path );
		$this->_loadConfigFiles();
		$this->init();
	}

	/**
	 * Sets the config path
	 * @param string $path
	 */
	public function setPath( $path ) {
		$path = rtrim( $path, '/' ) . '/';
		$this->_config_path = $path;
	}

	/**
	 * Returns the config path
	 * @return string
	 */
	public function getPath() {
		return $this->_config_path;
	}

	/**
	 * Return configs from config files
	 * @return array
	 */
	public function getConfigs() {
		return $this->_config;
	}

	/**
	 * Return post types from config array
	 * @return array
	 */
	public function getPostTypes() {
		return array_keys( $this->_config );
	}

	/**
	 * Return post types from config array
	 * @return array
	 */
	public function getPostTypeMetaboxes( $post_type ) {
		return $this->_config[ $post_type ]['metaboxes'];
	}

	/**
	 * Return all fields for post type
	 * @return array
	 */
	public function getAllFields( $post_type ) {
		return $this->_post_type_fields[ $post_type ];
	}

	/**
	 * Return post type taxonomies from config array
	 * @return array
	 */
	public function getPostTypeTaxonomies( $post_type ) {
		return isset( $this->_config[ $post_type ]['taxonomies'] ) ? $this->_config[ $post_type ]['taxonomies'] : array();
	}

	/**
	 * Register post types as defined in config file and start all this going
	 * @return void
	 */
	public function init() {
		$post_types = $this->getPostTypes();
		foreach ( $post_types as $post_type ) {
			$this->_post_type_fields[ $post_type ] = array();

			$metaboxes = $this->getPostTypeMetaboxes( $post_type );

			$taxonomies = $this->getPostTypeTaxonomies( $post_type );

			$current_post_type = new CWP_Post_Type();

			foreach ( $metaboxes as $metabox_id => $metabox ) {
				$title = $metabox['title'];
				$fields = $metabox['fields'];
				$position = ! empty( $metabox['position'] ) ? $metabox['position'] : 'normal';
				$priority = ! empty( $metabox['priority'] ) ? $metabox['priority'] : 'high';

				$this->_post_type_fields[ $post_type ] = array_merge( $this->_post_type_fields[ $post_type ], $fields );

				new CWP_Metabox( $current_post_type, $fields, $metabox_id, $title, $position, $priority );
			}

			$current_post_type->addPostType( $post_type );

			if ( count( $taxonomies ) ) {
				$taxnomy_obj = new CWP_Taxonomy();

				foreach ( $taxonomies as $taxonomy_name => $taxonomy ) {
					$taxnomy_obj->create( $current_post_type, $taxonomy_name, $taxonomy );
				}
			}

		}
	}

	/**
	 * Load config files
	 * @return void
	 */
	private function _loadConfigFiles() {
		if ( wpt()->options->isTransientCaching() && ( $config = get_transient('wpt_cwp_config') ) !== false ) {
			$this->_config = $config;
			return;
		}

		if ( ! is_dir( $this->_config_path ) ) {
				return;
		}

		$config_files = glob( $this->_config_path . '*', GLOB_NOSORT );

		foreach ( $config_files as $config_file ) {
			if ( strpos( $config_file, '.php' ) !== false ) {
				$post_type = current( explode( '.php', $config_file ) );

				$post_type = explode( '/', $post_type );

				$post_type = end( $post_type );

				$this->_config[ $post_type ] = @include $config_file;
			}
		}

		if ( wpt()->options->isTransientCaching() ) {
			set_transient( 'wpt_cwp_config', $this->_config, YEAR_IN_SECONDS );
		}
	}
}
