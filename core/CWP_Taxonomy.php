<?php
/**
 * CWP_Taxonomie class allows creating taxonomies
 *
 * @package  WPPostType
 */

class CWP_Taxonomy {
	/**
	 * Post type for taxnomie
	 * @var CWP_Post_Type
	 */
	private $_post_type;

	/**
	 * Taxonomie name
	 * @var string
	 */
	private $_taxonomy_name;

	/**
	 * Title of the args
	 * @var array
	 */
	private $_args;

	/**
	 * Initialize the metabox
	 * @param string $taxonomy_name
	 */
	public function __construct( $taxonomy_name = null ) {
		$this->setTaxonomy( $taxonomy_name );
	}

	/**
	 * Initialize the metabox
	 * @param CWP_Post_Type $post
	 * @param string $taxonomy_name
	 * @param array $args
	 */
	public function create( CWP_Post_Type $post, string $taxonomy_name, array $args = array() ) {
		$this->setTaxonomy( $taxonomy_name );
		$this->setArgs( $args );
		$this->setPostType( $post );
	}

	/**
	 * Set the post type of taxonomy
	 * @param CWP_Post_Type $post
	 */
	public function setPostType( CWP_Post_Type $post ) {
		$this->_post_type = $post;
		$this->_post_type->addTaxonomy( $this );
	}

	/**
	 * Set the taxonomy name
	 * @param string $taxonomy_name
	 */
	public function setTaxonomy( $taxonomy_name ) {
		$this->_taxonomy_name = $taxonomy_name;
	}

	/**
	 * Set the args of taxonomy
	 * @param array $setArgs
	 */
	public function setArgs( $args ) {
		$this->_args = $args;
	}

	/**
	 * Get the post type of taxonomy
	 * @return string
	 */
	public function getPostType() {
		return $this->_post_type;
	}

	/**
	 * Get the taxonomy name of metabox
	 * @return string
	 */
	public function getTaxonomy() {
		return $this->_taxonomy_name;
	}

	/**
	 * Get the args of taxonomy
	 * @return array
	 */
	public function getArgs() {
		return $this->_args;
	}

	/**
	 * Return all added taxonomys
	 * @param string $taxonomy
	 * @param boolean $hide_empty
	 * @param string $by (return by slug or id)
	 * @return array
	 */
	public static function getAll( $taxonomy = null, $by = 'id', $hide_empty = false ) {
		if ( is_null( $taxonomy ) ) {
			$taxonomy = $self::$_taxonomy_name;
		}

		$term_args = array(
		    'taxonomy'               => $taxonomy,
		    'hide_empty'             => $hide_empty,
		    'fields'                 => 'all',
		    'count'                  => true,
		);

		$term_query = new WP_Term_Query( $term_args );

		if ( $term_query->terms ) {
			if ( $by === 'slug' ) {
				foreach ( $term_query->terms as $term ) {
					$results[ $term->slug ] = $term->name;
				}
			} else {
				foreach ( $term_query->terms as $term ) {
					$results[ $term->term_id ] = $term->name;
				}
			}
		}

		return $results;
	}
}
