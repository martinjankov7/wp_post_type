<?php
/**
 * CWP_Fields class allows creating metabox
 *
 * @package  WPPostType
 */

class CWP_Fields {

	/**
	 * Holds all fields that need to be generated
	 * @var array
	 */
	private $_fields;

	/**
	 * Holds the fields html
	 * @var array
	 */
	private $_fields_html;

	/**
	 * Holds the post type name these fields are for
	 * @var string
	 */
	private $_post_type_name;

	/**
	 * Initililzes the process
	 * @param  array  $fields
	 * @return $this
	 */
	public function create( array $fields, string $post_type_name ) {
		$this->_fields_html = '';

		$this->setFields( $fields );
		$this->setPostTypeName( $post_type_name );
		$this->_createFields();

		return $this;
	}

	/**
	 * Sets fields variable
	 * @param array $fields
	 */
	public function setFields( array $fields) {
		$this->_fields = $fields;
	}

	/**
	 * Sets post type name variable
	 * @param string $post_type_name
	 */
	public function setPostTypeName( string $post_type_name) {
		$this->_post_type_name = $post_type_name;
	}

	/**
	 * Create the fields
	 */
	private function _createFields() {
		foreach ( $this->_fields as $name => $args ) {
			if ( empty( $args['type'] ) || empty ( $name ) ) continue;

			if ( $args['type'] == 'select' ) {
				$this->_createSelect( $name, $args );
			} elseif ( $args['type'] == 'textarea' ) {
				$this->_createTextarea( $name, $args );
			} else {
				$this->_createInput( $name, $args );
			}
		}
	}

	/**
	 * Creating of the select field type
	 * @param  string $name
	 * @param  array $args
	 * @return void
	 */
	private function _createSelect( $name, $args ) {
		if ( empty( $args['options'] ) && empty( $args['source'] ) && empty( $args['source_type'] ) ) {
			return;
		}

		$id = 'wpt-' . $this->_post_type_name . '-field-' . $name . ' ';
		$classes = $id . ! empty ( $args['class'] ) ? $args['class'] : '';

		$label = ! empty( $args['label'] ) ? $args['label'] : ucfirst( $name );

		$option_tags = '';

		if ( isset( $args['none_option'] ) && $args['none_option'] === true ) {
			$option_tags .= "<option value='-1'>--- None ---</option>";
		}

		$options = $args['options'];

		if ( ! empty( $args['source'] ) && ! empty( $args['source_type'] ) ) {
			$options = $this->_getDataFromSource( $args['source'], $args['source_type'] );
		}

		if ( count( $options ) === 0 ) {
			return;
		}

		foreach ( $options as $value => $title ) {
			$saved_value = CWP_Post_Type::get( $name );

			$selected = $value == $saved_value ? 'selected' : '';

			$option_tags .= "<option value='" . $value . "' " . $selected .">" . $title . "</option>";
		}

		$this->_fields_html .= "
			<div class='wpt-field'>
				<label for='" . $id . "'>". $label ."</label>
				<select name='" . $name . "' class='" . $classes . "' id='" . $id . "'>
				" . $option_tags . "
				</select>
			</div>
		";
	}

	private function _createTextarea( $name, $args ) {
		$id = 'wpt-' . $this->_post_type_name . '-field-' . $name . ' ';
		$classes = $id . ! empty ( $args['class'] ) ? $args['class'] : '';

		$label = ! empty( $args['label'] ) ? $args['label'] : ucfirst( $name );

		$saved_value = CWP_Post_Type::get( $name );

		$this->_fields_html .= "
			<div class='wpt-field'>
				<label for='" . $id . "'>". $label ."</label>
				<textarea  name='" . $name . "' class='" . $classes . "' id='" . $id . "' >'" . $saved_value . "'</textarea>
			</div>
		";
	}

	private function _createInput( $name, $args ) {
		$id = 'wpt-' . $this->_post_type_name . '-field-' . $name . ' ';
		$classes = $id . ! empty ( $args['class'] ) ? $args['class'] : '';

		$label = ! empty( $args['label'] ) ? $args['label'] : ucfirst( $name );

		$saved_value = CWP_Post_Type::get( $name );

		$additional_properties = '';

		if( $args['type'] == 'number' ) {
			if( ! empty( $args['min'] ) && is_numeric( $args['min'] ) ) {
				$additional_properties .= ' min="' . $args['min'] . '" ';
			}

			if( ! empty( $args['max'] ) && is_numeric( $args['max'] ) ) {
				$additional_properties .= ' max="' . $args['max'] . '" ';
			}
		}

		$this->_fields_html .= "
			<div class='wpt-field'>
				<label for='" . $id . "'>". $label ."</label>
				<input type='" . $args['type'] . "' name='" . $name . "' class='" . $classes . "' id='" . $id . "' " . $additional_properties . " value='" . $saved_value . "' />
			</div>
		";
	}

	/**
	 * Get data from dropdown source
	 * @param  string $source
	 * @param  string $source_type
	 * @return array
	 */
	private function _getDataFromSource( $source, $source_type ) {
		if ( $source_type === 'taxonomy' ) {
			$terms = CWP_Taxonomie::getAll( $source );
		} elseif( $source_type === 'post_type' ) {
			CWP_Post_Type::query( array( 'post_type' => $source ) );
			$terms = CWP_Post_Type::getIDTitleArr();
		} else {
			$terms = array();
		}

		return $terms;
	}

	/**
	 * Return the complete html
	 * @return string
	 */
	public function getHtml() {
		return "
			<div class='wpt-metabox-wrapper'>
			" . $this->_fields_html . "
			</div>
		";
	}
}
