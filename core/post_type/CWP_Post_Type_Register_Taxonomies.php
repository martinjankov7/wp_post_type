<?php
/**
 * CWP_Post_Type_Register_Taxonomies class allows creating new taxonomies for given post type
 *
 * @package  WPPostType
 */

class CWP_Post_Type_Register_Taxonomies {
	/**
	 * Taxonomie to be registered
	 * @var string
	 */
	private $_taxonomie_name;

	/**
	 * Post type for the taxonomie
	 * @var array
	 */
	private $_post_types;

	/**
	 * Arguments for the taxonomie
	 * @var array
	 */
	private $_args;

	/**
	 * Initialize the post type register
	 * @param string $taxonomie_name
	 * @param array $post_type
	 * @param array  $args
	 */
	public function __construct( string $taxonomie_name, array $post_types, array $args ) {
		$this->_taxonomie_name = $taxonomie_name;
		$this->_post_types = $post_types;
		$this->_args = $args;

		add_action( 'init', array( $this, 'register' ) );
	}

	/**
	 * Register the custom taxonomie
	 * @return void
	 */
	public function register() {
		$this->_validateArgs();

		$beautiful_name = ucfirst( preg_replace('/_|-/', ' ', $this->_taxonomie_name) );

		$labels = array(
			'name'              => __( $beautiful_name . 's', $this->_taxonomie_name ),
			'singular_name'     => __( $beautiful_name, $this->_taxonomie_name ),
			'search_items'      => __( 'Search ' . $beautiful_name . 's', $this->_taxonomie_name ),
			'all_items'         => __( 'All ' . $beautiful_name . 's', $this->_taxonomie_name ),
			'parent_item'       => __( 'Parent ' . $beautiful_name, $this->_taxonomie_name ),
			'parent_item_colon' => __( 'Parent ' . $beautiful_name, $this->_taxonomie_name ),
			'edit_item'         => __( 'Edit ' . $beautiful_name, $this->_taxonomie_name ),
			'update_item'       => __( 'Update ' . $beautiful_name, $this->_taxonomie_name ),
			'add_new_item'      => __( 'Add New ' . $beautiful_name, $this->_taxonomie_name ),
			'new_item_name'     => __( 'New ' . $beautiful_name . ' Name', $this->_taxonomie_name ),
			'menu_name'         => __( $beautiful_name . 's', $this->_taxonomie_name ),
		);
		$args = array(
			'hierarchical'      => $this->_args['hierarchical'],
			'labels'            => $labels,
			'show_ui'           => $this->_args['show_ui'],
			'show_admin_column' => $this->_args['show_admin_column'],
			'query_var'         => $this->_args['query_var'],
			'publicly_queryable' => $this->_args['publicly_queryable'],
			'rewrite'           => $this->_args['rewrite'],
		);
		register_taxonomy( $this->_taxonomie_name, $this->_post_types, $args );
	}

	/**
	 * Loop through all metaboxes and add them to the post type
	 * @return void
	 */
	public function metaboxes() {
		foreach ( $this->_metaboxes as $metabox ) {
			add_meta_box(
				$metabox->getId(),
				$metabox->getTitle(),
				array( $metabox, 'fields' ),
				$this->_post_type,
				$metabox->getPosition(),
				$metabox->getPriority(),
				$metabox->getCallbackArgs()
			);
		}
	}

	/**
	 * Add default values if some argument is missing
	 * @return void
	 */
	private function _validateArgs() {
		if ( empty ( $this->_args['hierarchical'] ) ) {
			$this->_args['hierarchical'] = true;
		}

		if ( empty ( $this->_args['show_ui'] ) ) {
			$this->_args['show_ui'] = true;
		}

		if ( empty ( $this->_args['show_admin_column'] ) ) {
			$this->_args['show_admin_column'] = true;
		}

		if ( empty ( $this->_args['query_var'] ) ) {
			$this->_args['query_var'] = true;
		}

		if ( empty ( $this->_args['publicly_queryable'] ) ) {
			$this->_args['publicly_queryable'] = true;
		}

		if ( empty ( $this->_args['rewrite'] ) ) {
			$this->_args['rewrite'] = array( 'slug' => $this->_taxonomie_name );
		}
	}
}
