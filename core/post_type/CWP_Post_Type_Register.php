<?php
/**
 * CWP_Post_Type_Register class allows creating new post type
 * You can add additional filters and action by using this class as parent
 *
 * @package  WPPostType
 */

class CWP_Post_Type_Register {
	/**
	 * Post type to be registered
	 * @var string
	 */
	private $_post_type;

	/**
	 * Arguments for the post type
	 * @var array
	 */
	private $_args;

	/**
	 * Metaboxes for this post type
	 * @var array
	 */
	private $_metaboxes;

	/**
	 * Initialize the post type register
	 * @param string $post_type
	 * @param array  $args
	 * @param array  $metaboxes
	 */
	public function __construct( string $post_type, array $args, array $metaboxes = array() ) {
		$this->_post_type = $post_type;
		$this->_args = $args;
		$this->_metaboxes = $metaboxes;

		add_action( 'init', array( $this, 'register' ) );
	}

	/**
	 * Register the custom post type
	 * @return void
	 */
	public function register() {
		$this->_validateArgs();

		$labels = array(
			'name'               => __( $this->_args['name'], $this->_post_type ),
			'singular_name'      => __( $this->_args['name'], $this->_post_type ),
			'add_new'            => __( 'Create New ' . $this->_args['name'], $this->_post_type ),
			'add_new_item'       => __( 'Create New ' . $this->_args['name'], $this->_post_type ),
			'edit_item'          => __( 'Edit ' . $this->_args['name'], $this->_post_type ),
			'new_item'           => __( 'Create New ' . $this->_args['name'], $this->_post_type ),
			'all_items'          => __( 'All ' . $this->_args['name'] . 's', $this->_post_type ),
			'view_item'          => __( 'View ' . $this->_args['name'], $this->_post_type ),
			'search_items'       => __( 'Search ' . $this->_args['name'] . 's', $this->_post_type ),
			'not_found'          => __( 'No ' . $this->_args['name'] . 's found', $this->_post_type ),
			'not_found_in_trash' => __( 'No ' . $this->_args['name'] . 's found in the Trash', $this->_post_type ),
			'parent_item_colon'  => '',
			'menu_name'          => __( $this->_args['name'] . 's', $this->_post_type )
		);
		$post_type_args = array(
			'labels'        => $labels,
			'description'   => __( 'Showing ' . $this->_args['name'] .'s', $this->_post_type ),
			'public'        => $this->_args['public'],
			'menu_position' => $this->_args['menu_position'],
			'supports'      => $this->_args['supports'],
			'has_archive'   => true,
			'register_meta_box_cb' => array( $this, 'metaboxes' )
		);

		if ( $this->_args['with_capabilities'] ) {
			$post_type_args['capabilities'] = array(
			    'edit_post'          => 'edit_' . $this->_post_type,
			    'read_post'          => 'read_' . $this->_post_type,
			    'delete_post'        => 'delete_' . $this->_post_type,
			    'edit_posts'         => 'edit_' . $this->_post_type,
			    'edit_others_posts'  => 'edit_others_' . $this->_post_type,
			    'delete_posts'       => 'delete_' . $this->_post_type,
			    'publish_posts'      => 'publish_' . $this->_post_type,
			    'read_private_posts' => 'read_private_' . $this->_post_type
			);
		}

		register_post_type( $this->_post_type, $post_type_args );
	}

	/**
	 * Loop through all metaboxes and add them to the post type
	 * @return void
	 */
	public function metaboxes() {
		if ( count( $this->_metaboxes ) ) {
			foreach ( $this->_metaboxes as $metabox ) {
				add_meta_box(
					$metabox->getId(),
					$metabox->getTitle(),
					array( $metabox, 'fields' ),
					$this->_post_type,
					$metabox->getPosition(),
					$metabox->getPriority(),
					$metabox->getCallbackArgs()
				);
			}
		}
	}

	/**
	 * Add default values if some argument is missing
	 * @return void
	 */
	private function _validateArgs() {
		if ( empty ( $this->_args['name'] ) ) {
			$this->_args['name'] = ucfirst( $this->_post_type );
		}

		if ( empty ( $this->_args['menu_position'] ) ) {
			$this->_args['menu_position'] = 5;
		}

		if ( empty ( $this->_args['public'] ) ) {
			$this->_args['public'] = true;
		}

		if ( empty ( $this->_args['supports'] ) ) {
			$this->_args['supports'] = array( 'title', 'editor', 'thumbnail' );
		}

		if ( empty ( $this->_args['with_capabilities'] ) ) {
			$this->_args['with_capabilities'] = false;
		}
	}

}
