<?php
/**
 * CWP_Post_Type class allows creating new post type
 * You can add additional filters and action by using this class as parent
 * Implements CWP_Post_Type_Interface
 *
 * @package  WPPostType
 */

class CWP_Post_Type implements CWP_Post_Type_Interface {

	/**
	 * The post type to register
	 * @var string
	 */
	private $_post_type;

	/**
	 * Post type name
	 * @var string
	 */
	private $_post_type_name;

	/**
	 * All posts types to be registered
	 * @var array
	 */
	private $_post_types;

	/**
	 * Arguments to pass when regisitering the post type
	 * @var array
	 */
	protected $args;

	/**
	 * Metaboxes to add to the post type
	 * @var array
	 */
	private $_metaboxes;

	/**
	 * Keeps the result from the query
	 * @var mixed (array | object)
	 */
	private static $_query_result;

	/**
	 * Setting initial values for the variables and adding actions
	 * @param string $post_type
	 * @param CWP_Config $config
	 * @param array  $args
	 */
	public function __construct() {
		$this->_metaboxes = array();
		$this->_post_types = array();

		add_action( 'save_post', array( $this, 'onSave' ), 20, 3 );
	}

	/**
	 * Setting initial values for the variables and adding actions
	 * @param string $post_type
	 * @param array  $args
	 * @return  $this
	 */
	public function addPostType( string $post_type, array $args = array() ) {
		$this->_post_type_name = $post_type;
		new CWP_Post_Type_Register( $post_type, $args, $this->getMetaboxes() );
	}

	/**
	 * Add metabox to the list
	 * @param CWP_Metabox $metabox
	 */
	public function addMetabox( CWP_Metabox $metabox ) {
		$this->_metaboxes[] = $metabox;
	}

	/**
	 * Register taxonomy
	 * @param CWP_Taxonomy $taxonomy
	 */
	public function addTaxonomy( CWP_Taxonomy $taxonomy ) {
		new CWP_Post_Type_Register_Taxonomies(
			$taxonomy->getTaxonomy(),
			array( $this->getPostTypeName() ),
			$taxonomy->getArgs()
		);
	}

	/**
	 * Returns all metaboxes
	 * @return array Array of metaboxes
	 */
	public function getMetaboxes() {
		return $this->_metaboxes;
	}

	/**
	 * Triggers on save of post
	 * To be overwritten in child classes if needed
	 *
	 * @param  int     $post_id
	 * @param  WP_Post $post
	 * @param  bool    $update
	 * @return void
	 */
	public function onSave( int $post_id, WP_Post $post, bool $update ) {
		$post_types = wpt()->config->getPostTypes();

		if ( ! in_array( $post->post_type, $post_types ) ) {
			return $post_id;
		}

		$fields = wpt()->config->getAllFields( $post->post_type );

		foreach ( $fields as $name => $args ) {
			$meta_key = $args['meta_key'];
			$value = ! empty( $_POST[ $name ] ) ? $_POST[ $name ] : false;

			if ( empty( $meta_key ) ) {
				$meta_key = '_' . $name;
			}
			if ( $value !== false ) {
				if ( get_post_meta( $post_id, $meta_key, true ) ) {
					update_post_meta( $post_id, $meta_key, $value );
				} else {
					add_post_meta( $post_id, $meta_key, $value );
				}
			} else {
				if ( get_post_meta( $post_id, $meta_key, true ) ) {
					delete_post_meta( $post_id, $meta_key );
				}
			}
		}
	}

	/**
	 * Return current post type name
	 */
	public function getPostTypeName() {
		return $this->_post_type_name;
	}

	/**
	 * Get post type custom field value
	 * @param  string $field
	 * @param  int $ID
	 * @return string
	 */
	public static function get( string $field_name, int $ID = null ) {
		if ( is_null( $ID ) ) {
			$ID = get_the_ID();
		}

		global $post;

		$field = wpt()->config->getAllFields( $post->post_type )[ $field_name ];

		$meta_key = ! empty ( $field['meta_key'] ) ? $field['meta_key'] : '_' . $field_name;

		if ( ! empty( $meta_key ) ) {
			return get_post_meta( $ID, $meta_key, true );
		}

		return '';
	}

	/**
	 * Query wrapper
	 * @param  array  $args
	 * @return array
	 */
	public static function query( array $args = array() ) {
		$query_args = array(
			'post_type' => ! empty( $args['post_type'] ) ? $args['post_type'] : 'any',
			'status' => 'publish',
		);

		if ( ! empty( $args['per_page'] ) && is_numeric( $args['per_page'] ) ) {
			$query_args['posts_per_page'] = $args['per_page'];
		}

		if ( ! empty( $args['page'] ) && is_numeric( $args['page'] ) ) {
			$query_args['paged'] = $args['page'];
		}

		if ( ! empty( $args['include'] ) && is_array( $args['include'] ) ) {
			$query_args['post__in'] = $args['include'];
		}

		if ( ! empty( $args['exclude'] ) && is_array( $args['exclude'] ) ) {
			$query_args['post__not_in'] = $args['exclude'];
		}

		if ( ! empty( $args['meta_query'] ) && is_array( $args['meta_query'] ) ) {
			$query_args['meta_query'] = $args['meta_query'];
		}

		$query = new WP_Query( $query_args );

		self::$_query_result = array();

		if ( $query->have_posts() ) {
				self::$_query_result =  $query->posts;
		}

		return self::$_query_result;
	}

	/**
	 * Return formated array with post id and title
	 * @return array
	 */
	public static function getIDTitleArr() {
		$query_result = self::$_query_result;

		$result = array();

		foreach ( $query_result as $record ) {
			$result[ $record->ID ] = $record->post_title;
		}

		return $result;
	}
}
