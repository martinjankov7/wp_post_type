<?php
/**
 * CWP_Admin_Dashboard class admin options
 *
 * @package    WPPostType
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class CWP_Admin_Dashboard {
	public function __construct() {
		add_action('admin_init', array($this, 'register_settings') );
		add_action('admin_menu', array($this, 'menu') );
	}

	public function menu() {
		add_menu_page('Custom Post Type', 'Custom Post Type', 'administrator', 'wpt-settings', array( $this,'settings' ), 'dashicons-admin-generic');
	}

	public function settings() {
		@include_once WPT_PLUGIN_DIR . 'views/admin/template-settings.php';
	}

	public function register_settings() {
		register_setting( 'wpt-settings-group', 'wpt_config_cache' );
		register_setting( 'wpt-settings-group', 'wpt_memcache_host' );
		register_setting( 'wpt-settings-group', 'wpt_memcache_port' );
	}

}
